<?php 
include('functions.php');
  if (!isLoggedIn()) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');}
//Database Connection
ini_set ("error_reporting",  E_ALL & ~E_NOTICE & ~E_USER_NOTICE);
include('dbconnection.php');
if(isset($_POST['submit']))
  { 
  	$eprofileid=$GET['editprofileid']?? '';
  	//Getting Post Values
    $username=$_POST['username'];
    $fullname=$_POST['fullname'];
    $mobile=$_POST['mobile'];
    $email=$_POST['email'];
    $address=$_POST['address'];

    //Query for data updation
     $query=mysqli_query($con, "update  users set username='$username', fullname='$fullname', mobile='$mobile', email='$email', address='$address'   where id='$eprofileid'");
     
    if ($query) {
    echo "<script>alert('You have successfully update the data2');</script>";
    echo "<script type='text/javascript'> document.location ='userprofile.php'; </script>";
  }
  else
    {
      echo "<script>alert('Something Went Wrong. Please try again');</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
<title>User Profile</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
body {
	color: #fff;
	background: #38b6ff;
	font-family: 'Roboto', sans-serif;
}
.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}
.form-control:focus {
	border-color: #5cb85c;
}
.form-control, .btn {        
	border-radius: 3px;
}
.signup-form {
	width: 450px;
	margin: 0 auto;
	padding: 30px 0;
  	font-size: 15px;
}
.signup-form h2 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}
.signup-form h2:before, .signup-form h2:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}	
.signup-form h2:before {
	left: 0;
}
.signup-form h2:after {
	right: 0;
}
.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}
.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}
.signup-form .form-group {
	margin-bottom: 20px;
}
.signup-form input[type="checkbox"] {
	margin-top: 3px;
}
.signup-form .btn {        
	font-size: 16px;
	font-weight: bold;		
	min-width: 140px;
	outline: none !important;
}
.signup-form .row div:first-child {
	padding-right: 10px;
}
.signup-form .row div:last-child {
	padding-left: 10px;
}    	
.signup-form a {
	color: #fff;
	text-decoration: underline;
}
.signup-form a:hover {
	text-decoration: none;
}
.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}	
.signup-form form a:hover {
	text-decoration: underline;
}  
</style>
</head>
<body>
<div class="signup-form">
    <form  method="POST">
 <?php
error_reporting (E_ALL ^ E_NOTICE);

$ret=mysqli_query($con,"select * from users where id='".$_SESSION['user']['id']."'");
while ($row=mysqli_fetch_array($ret)) {
?>
		<h2>Update </h2>
		<p class="hint-text">Update User Profile</p>

	<div class="form-group">
<img src="userprofiles/<?php  echo $row['userPP'];?>" width="200" height="120">
<a href="change-image-profile.php?id=<?php echo $row['id'];?>">Change Image</a>
		</div>

      <div class="form-group">
			<div class="row">
				<div class="col">Username<input type="text" class="form-control" name="username" value="<?php  echo $row['username'];?>" required="true"></div>

				<div class="col">Fullname<input type="text" class="form-control" name="fullname" value="<?php  echo $row['fullname'];?>" required="true"></div>
			</div>
		</div>
				<div class="form-group">
				<div class="row">
				<div class="col">Mobile<input type="text" class="form-control" name="mobile" value="<?php  echo $row['mobile'];?>" required="true"></div>
				<div class="col">Email<input type="email" class="form-control" name="email" value="<?php  echo $row['email'];?>" required="true"></div>
			</div>        	
     </div>
        <div class="form-group">
         <div class="col">Address<input type="text" class="form-control" name="address" value="<?php  echo $row['address'];?>" required="true">
        </div>
      </div>

<?php 
}?>
		<div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block" name="submit" style="background-color:#38b6ff;">Update</button>
        </div>
        <div class="text-center"><a href="userprofile.php" style="text-decoration: none;">&#8592;Go Back</a></div>
    </form>

</div>
</body>
</html>