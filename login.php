<?php include('functions.php') ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
   	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>User Registration</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background-image: url(images/bg.png); background-size: cover; font-family: sans-serif;">
	
	<form method="post" action="login.php" style="background-color: #FFFFF0; ">

		<?php echo display_error(); ?>

		<h1 style="text-align: center; color: #5b6574; font-size: 35px;">Login</h1>
		<hr style="margin: 10px;">
		
		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" placeholder="Username">
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password" placeholder="Password">
		</div>
		<div class="input-group">
			<center><button type="submit" class="btn" name="login_btn">Login</button></center>
		</div>
		<center>
		<p>
			Forgot Password? <a href="register.php">Click Here</a><br>
			New to Caniline? <a href="register.php">Sign up</a>
		</p></center>
	</form>
</body>
</html>