/*
Locations
*/
const tilesProvider ='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
let myMap = L.map('myMap').setView([10.6723,122.9511], 13);
L.tileLayer(tilesProvider,{maxZoom: 18,}).addTo(myMap);

const locations = [
	  [{lat: 10.66912332,  lng:122.9580635},'Zitro Vet Clinic Orsons Bldg. Cor. Lopez Jaena-, Burgos Ave, Bacolod, Negros Occidental',1],
	  [{lat: 10.65259088, lng: 122.9607028},'Red Marks Animal Clinic Villa Celia Subdivision, Blk 2 Lot 2, Bacolod, 6100 Negros Occidental',2],
	  [{lat: 10.64903924, lng:122.9652616},'Green Paws Veterinary Clinic Lot 3, Block 1, Eufemia Compound, Circumferential Rd, Bacolod, 6100 Negros Occidental',3],
	  [{lat: 10.64301683, lng: 122.9454464},'Dr. S&J Veterinary Clinic and Grooming Door 2, Garces Bldg, Alijis Rd, Bacolod, 6100 Negros Occidental',4],
	  [{lat: 10.68988356, lng: 122.9587087},'Mandalagan Veterinary Clinic Lacson St, Bacolod, Negros Occidental',5],
	  [{lat: 10.70573668, lng: 122.9616468 },'Pet House Central Veterinary Clinic Carlos Hilado Hwy, Bacolod, 6100 Negros Occidental',6],
	  [{lat: 10.67102112, lng: 122.9542011},'St. Jude Dog & Cat Veterinary Clinic Burgos Ave, Bacolod, 6100 Negros Occidental',7],
	  [{lat: 10.64075891, lng: 122.9708413},'De Perro Pet Clinic Victorina Heights, Door 4 and 5, Block 3, Lot 1, Bacolod, 6100 Negros Occidental',8],
	  [{lat: 10.6803862, lng: 122.9907937},'Pet Medics Veterinary Clinic Lungsod ng Bacolod, 6100 Negros Occidental',9],
	  [{lat: 10.67815093, lng: 122.9547275},'Pet Connection Veterinary Clinic #6, 11th St, Lacson St, Bacolod, 6110 Negros Occidental',10],
	  [{lat: 10.66494812, lng:122.9430861 },'Santoceldes Animal Clinic 40-2 Ferrer Building, Hernaez St, Bacolod, 6100 Negros Occidental',11],
	  [{lat: 10.64222148, lng: 122.9331468 },'Puss & Pooch Veterinary Clinic Automatic Climate Control, Bacolod, 6100 Negros Occidental',12],
	  [{lat: 10.67266587, lng:122.9566902 },'Bacolod Animal Hospital and Veterinary Supply MXC4+PM4, Bacolod, 6100 Negros Occidental',13],
	  [{lat: 10.6110531, lng:122.9249682 },'HappyFeet Veterinary Clinic Guadalupe Building, Araneta Ave, Bacolod, 6100 Negros Occidental',14],
	  [{lat: 10.66518719, lng:122.9481639 },'Animal World Veterinary Clinic 51-A Lacson St., Corner San Sebastian Street, Bacolod, 6100 Negros Occidental',15],
	  [{lat: 10.70581545, lng:122.9621572 },'Bacolod Dog and Cat Clinic PX36+XPQ, Carlos Hilado Hwy, Bacolod, Negros Occidental',16],
	  [{lat: 10.65318134, lng:122.9593939 },'Bacolod Dog and Cat Clinic #2 P Hernaez St Ext, Bacolod, 6100 Negros Occidental',17],
	  [{lat: 10.66193158, lng:122.9949303 },'Vet Next Door Veterinary Services MX6V+CWC, Burgos Ave, Bacolod, 6100 Negros Occidental',18],
	  [{lat: 10.6639339, lng:122.9690056 },'Juan Veterinarian - Bacolod Burgos Extension, Bacolod, 6100 Negros Occidental',19],
	  [{lat: 10.7446717, lng:122.9693317 },'Royal K9 Animal Clinic PXV9+GM3, Paseo Mabini, Brgy. Zone II, Talisay City, 6115 Negros Occidental',20],
	  [{lat: 10.60438046, lng:122.9199381 },'Animal Clinic JW39+CXG, Villarosa St, Bacolod, Negros Occidental',21],
	  [{lat: 10.671611, lng:122.9638797 },'Sanfegi Veterinary Clinic GRN Glass Bldg, BS Aquino Dr, Brgy Villamonte, Bacolod, 6100 Negros Occidental',22],
	  [{lat: 10.66321898, lng: 122.9474205},'B. Delorino Veterinary Clinic & Supply 94A Lacson St, Bacolod, 6100 Negros Occidental',23],
	  [{lat: 10.6631768, lng:122.9474205 },'Lori Pet Care Products 96 Lacson St, Bacolod, 6100 Negros Occidental',24],
	  [{lat: 10.6568506, lng:122.9564327 },'Dr. Eliezer Dela Cruz 40-2 P Hernaez St Ext, Bacolod, 6100 Negros Occidental',25],
	  [{lat: 10.66695292, lng:122.9636595 },'Most Valuable Pet (MVP) Vet Clinic MX87+FC6, Bacolod, 6100 Negros Occidental',26],
	  [{lat: 10.63172944, lng:122.9768854 },'Pet Village JXJG+8MC, P Hernaez Street, Extension, Bacolod, 6100 Negros Occidental',27],
	  [{lat: 10.80160935, lng:122.9788365 },'Primavet - Animal Clinic DOOR #1, MONTINOLA BLG. (near STL Silay, Bonifacio St, Silay City, 6116 Negros Occidental',28],
	  [{lat: 10.61607532, lng:122.9223194 },'Animal Barracks Clinic JW3C+93G, Bacolod, Negros Occidental',29],
	  [{lat: 10.69535786, lng:122.95836 },'Novartis Animal Health MXF5+4Q2, Yakal St, Bacolod, 6100 Negros Occidental',30],
	  [{lat: 10.61270084, lng:122.9223194 },'Monreal Veterinary Trading HWW9+9QH, Peñafrancia St, Bacolod, Negros Occidental',31],
	  [{lat: 10.68288238, lng:122.9896107 },'Barking Lot Veterinary Clinic Bacolod, 6100 Negros Occidental',32],
	  [{lat: 10.69502754, lng:122.9518452 },'Provincial Veterinary Office MXH2+7W3, Lungsod ng Bacolod, 6100 Negros Occidental',33]
	];

for (let i = 0; i < locations.length; i++) {
	const element = locations[i];
	let lat = element[0]['lat'];
	let lng = element[0]['lng'];
	let suc = element[1];
	let iconX = element[2];	
	const urlimg = "assets/img/icon0"+iconX+".png";	
	
	let iconM = L.divIcon({
							className:'iconMarker icon'+iconX,
							html: '<span></span>',
							popupAnchor:[33,33]
						});
	L.marker([lat,lng],{icon: iconM, title: suc}).addTo(myMap).bindPopup('<h2>'+suc+'</h2>');	
}