<?php 
  include('functions.php');
  if (!isLoggedIn()) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Caniline</title>
  
  <meta content="" name="description">
  <meta content="" name="keywords">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">                                           

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Squadfree - v4.9.1
  * Template URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<style>
body{
    margin-top:20px;
    background-color: white;
   
}
/* ===========
   Profile
 =============*/
.card-box {
  padding: 20px;
  box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.06), 0 2px 0 0 rgba(0, 0, 0, 0.02);
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  background-clip: padding-box;
  margin-bottom: 20px;
  background-color: #ffffff;
}
.header-title {
  text-transform: uppercase;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.04em;
  line-height: 16px;
  margin-bottom: 8px;
}
.social-links li a {
  -webkit-border-radius: 50%;
  background: #EFF0F4;
  border-radius: 50%;
  color: #7A7676;
  display: inline-block;
  height: 30px;
  line-height: 30px;
  text-align: center;
  width: 30px;
}

/* ===========
   Gallery
 =============*/
.portfolioFilter a {
  -moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -moz-transition: all 0.3s ease-out;
  -ms-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -webkit-transition: all 0.3s ease-out;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  color: #333333;
  padding: 5px 10px;
  display: inline-block;
  transition: all 0.3s ease-out;
}
.portfolioFilter a:hover {
  background-color: #228bdf;
  color: #ffffff;
}
.portfolioFilter a.current {
  background-color: #228bdf;
  color: #ffffff;
}
.thumb {
  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  margin-top: 30px;
  padding-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  width: 100%;
}
.thumb-img {
  border-radius: 2px;
  overflow: hidden;
  width: 100%;
}
.gal-detail h4 {
  margin: 16px auto 10px auto;
  width: 80%;
  white-space: nowrap;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
}
.gal-detail .ga-border {
  height: 3px;
  width: 40px;
  background-color: #228bdf;
  margin: 10px auto;
}


.tabs-vertical-env .tab-content {
  background: #ffffff;
  display: table-cell;
  margin-bottom: 30px;
  padding: 30px;
  vertical-align: top;
}
.tabs-vertical-env .nav.tabs-vertical {
  display: table-cell;
  min-width: 120px;
  vertical-align: top;
  width: 150px;
}
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  background-color: #ffffff;
  border: 0;
}
.tabs-vertical-env .nav.tabs-vertical li > a {
  color: #333333;
  text-align: center;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  white-space: nowrap;
}
.nav.nav-tabs > li.active > a {
  background-color: #ffffff;
  border: 0;
}
.nav.nav-tabs > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  font-weight: 500;
  padding-left: 20px;
  padding-right: 20px;
  font-family: 'Roboto', sans-serif;
}
.nav.nav-tabs > li > a:hover {
  color: #228bdf !important;
}
.nav.tabs-vertical > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  padding-left: 20px;
  padding-right: 20px;
}
.nav.tabs-vertical > li > a:hover {
  color: #228bdf !important;
}
.tab-content {
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
  color: #777777;
}
.nav.nav-tabs > li:last-of-type a {
  margin-right: 0px;
}
.nav.nav-tabs {
  border-bottom: 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}
.navtab-custom li {
  margin-bottom: -2px;
}
.navtab-custom li a {
  border-top: 2px solid transparent !important;
}
.navtab-custom li.active a {
  border-top: 2px solid #228bdf !important;
}
.nav-tab-left.navtab-custom li a {
  border: none !important;
  border-left: 2px solid transparent !important;
}
.nav-tab-left.navtab-custom li.active a {
  border-left: 2px solid #228bdf !important;
}
.nav-tab-right.navtab-custom li a {
  border: none !important;
  border-right: 2px solid transparent !important;
}
.nav-tab-right.navtab-custom li.active a {
  border-right: 2px solid #228bdf !important;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus,
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  border: none;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:focus,
.nav-tabs > li.active > a:hover,
.tabs-vertical > li.active > a,
.tabs-vertical > li.active > a:focus,
.tabs-vertical > li.active > a:hover {
  color: #228bdf !important;
}

.nav.nav-tabs + .tab-content {
    background: #ffffff;
    margin-bottom: 20px;
    padding: 20px;
}
.progress.progress-sm .progress-bar {
    font-size: 8px;
    line-height: 5px;
}
*, *::before, *::after {
  box-sizing: border-box;
  margin: 0;
}

:root {
  font-size: 62.5%;
  --light-gray: rgb(162, 169, 177);
  --blue: rgb(6, 69, 173);
  --white-bone: rgb(248, 249, 250);
  --light-black: rgb(32, 33, 34);
}

body {
  padding: 2em;
  
  font-family: sans-serif;
}
img {animation: fadeInAnimation ease 3s;
            animation-iteration-count: 1;
            animation-fill-mode: forwards;
        }
        @keyframes fadeInAnimation {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

.main-title {
  padding: .5em 0;
  border-bottom: 1px solid var(--light-gray);
  margin-bottom: 30px;

  font-family: 'Georgia', serif;
  font-size: clamp(2.3rem, 3vw, 2.88rem);
  font-weight: 500;
  color: black;
}

.table-of-contents {
  max-width: 500px;
  padding: .75ch;
  font-size: 2rem;
  
}

.table-narrow {
  width: 160px;
}

.list-invisible {
  display: none;
}

.wrap {
  display: flex;
  justify-content: center;
  align-items: center;
  
  margin: 4px 0;
}

.table__title {
  margin-right: .5ch;
  
  font-size: 1.5rem;
}

.toggle-wrapper::before {
  content: '[';
  
  color: var(--light-black);
}

.toggle-wrapper::after {
  content: ']';
  
  color: var(--light-black);
}

.toggle {
  padding: 0;
  border: none;
  
  font-size: 1.5rem;
  
  color: var(--blue);
  background-color: inherit;
}

.toggle:hover {
  text-decoration: underline var(--blue);
  cursor: pointer;
}

.toggle:focus {
  border: 1px dotted var(--blue);
  
  text-decoration: underline var(--blue);
}

.toggle:active {
  border: none;
  text-decoration: none;
}

.table__list,
.table__nested-list {
  list-style: none;
}

.content__number {
  margin-right: .5ch;
  
  color: var(--light-black);
}

.table__list {
  padding: 0;
}

.table__nested-list {
  padding-left: 3ch;
}

.table__link {
  color: var(--black);
  
  font-weight: 500;
  line-height: 1.6;
  
  text-decoration: none;
}

.table__link:active {
  color: rgb(245, 194, 66);
}

.table__link:hover {
  text-decoration: underline var(--blue);
}
.vl {
  border-left: 6px solid black;
  height: 1270px;
  margin-left: 75%;
  margin-top: -285%;
  opacity: 0.5;}

}
</style>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-image: url(images/full.png)">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1 class="text-light"><a href="index.php"><img src="images/caniline.png" alt="" class="img-fluid" ><span>Caniline</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

     <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="index.php">Home</a></li>
          <li><a class="nav-link scrollto" href="userprofile.php">User Profile</a></li>
          <li><a class="nav-link scrollto" href="symptomcheck1.php">Symptom Check</a></li>
          <li><a class="nav-link scrollto" href="vetlocation.php">Vet Location</a></li>
          <li class="dropdown"><a href="caretips.php"><span>Care Tips</span></i></a>
          </li>
          <li class="dropdown"><a href="index.php?logout='1'"><span>Log out</span> <i class="bi bi-chevron-down"></i></a>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

<main id="main">

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <link rel="stylesheet" href="style.css"> 
</head>
<body>
<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat');

* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
body {
}

.accordion {
  width: 60%;
  max-width: 1000px;
  margin: 2rem auto;
}
.accordion-item {
  background-color: #fff;
  color: #292f33;
  margin: 1rem 0;
  border-radius: 0.3rem;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,0.25);
}
.accordion-item-header {
  padding: 0.5rem 3rem 0.5rem 1rem;
  min-height: 3.5rem;
  line-height: 1.25rem;
  font-weight: bold;
  display: flex;
  font-size: 17px;
  align-items: center;
  position: relative;
  cursor: pointer;
}
.accordion-item-header::after {
  content: "\002B";
  font-size: 2rem;
  position: absolute;
  right: 1rem;
}
.accordion-item-header.active::after {
  content: "\2212";
}
.accordion-item-body {
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
.accordion-item-body-content {
  text-align: justify;
  padding: 1rem;
  line-height: 1.5rem;
  font-family: sans-serif;
  border-top: 1px solid;
  font-size: 16px;
  border-image: linear-gradient(to right, transparent, #34495e, transparent) 1;
}

@media(max-width:767px) {
  html {
    font-size: 14px;
  }
}
</style>
 <main>
<!-- <a target="_blank" href="images/first.jpg">
<img src="images/first.jpg" style="width: 500px; height: 444px; float: right; "></a> -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!----CSS link----->
    <link rel="stylesheet" href="style.css"> 
</head>
<div style="background-color: #cadfdf; width: 100%; height: 500px; padding: 50px;">
 

  <img src="images/first.jpg" style="width: 600px; float: right;">
  </div>
  <h1 style="font: small-caps bold 100px/1 sans-serif; margin-top: -300px; text-align: center;">Care Tips</h1>
  <br>

  <nav class="section-nav" style="font-size: 18px; margin-top: 250px; margin-left: 50px;">


        <ol>
          <li><a href="#generaltips">General Tips</a></li>
          <li><a href="#lifestage">Based on Life Stage</a></li>
            <ul>
              <li class=""><a href="#endpoints--puppy">Puppy</a></li>
              <li class=""><a href="#endpoints--adult">Adult</a></li>

            </ul>
          </li>
          <li class=""><a href="#conditions">Based on Conditions</a></li>
          <li class=""><a href="#size">Based on Size</a></li>
<ul>
              <li class=""><a href="#endpoints--root">Small Breed</a></li>
              <li class=""><a href="#endpoints--cities-overview">Large Breed</a></li>
              
            </ul>
        </ol>
      </nav>
<div>
<section id="generaltips" style="width: 100%; margin-top: 50px;">
  <!-- <img src="images/slider-img.png" style="width: 400px; height: 333px; float: right; padding: 5px;"> -->

<h1 style="font-size: 30px;"><b>General Tips</b></h1>
<br>
<p style="font-size: 18px; width: 60%; text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;As a dog owner, you may adore your dog as if it were your own flesh and blood. And just like your children, you want to ensure you provide the best care possible so your dog can live a happy and healthy life. If you’re striving to be an excellent pet parent, here are some dog care tips to help you along the way.The following are basic five-point check as a tip to taking care a dog you can do. <br>

<br><b>1. </b>Check for your dog weight regularly, at least once per month to check if your dog weight is still healthy, slightly overweight, or underweight.<br>
<br><b>2.</b> Check for the signs of fleas -black flakes or specks to maintain healthy coat and skin.<br>
<br><b>3. </b> Gently pull down the lower eyelid to check for a pink colour. The whites of the eye should be glossy white with no redness. For their ears, it should be clean in a pink colour, free of debris and strong odors.<br>
<br><b>4. </b> Open the dog's mouth to inspect all his teeth. Beware of tartar build-up, this should be removed by a veterinarian and chew snacks designed to help reduce plaque and tartar build-up like Pedigree® Dentastix®. Brushing your dog’s teeth regularly is also a great way to keep his gums and teeth healthier and fight gum disease.<br>
<br><b>5.</b> Check for unusual lumps or bumps by placing both hands on top of your dog's head and moving down under the chin. Next, move your hands behind the front legs, under the shoulders, down the back, over the hips, and down the legs. Inspect your dog's claws and footpads for cuts or cracks. Report unusual symptom to a veterinarian.<br></p>
</section>
</div>
<section id="lifestage" style="margin-top: 50px; ">
<br>
<div style="padding: 20px; margin-left: 560px;"><label for="filter"style="font-size: 16px;">Search:</label> <input type="text" name="Search" value="" id="filter" style="font-size: 16px;"/></div>

<h1 style="font-size: 30px;"><b>Based on Life Stage</b></h1>
<br>
<h2 style="padding-bottom: 20px;">Puppy</h2>

  <body id="index" TOPMARGIN=0 LEFTMARGIN=0>
    <div id="pagewrap">
      <div id="body" style="width: 70%; font-size: 16px; margin-bottom: 20px;">
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=100%>
          <thead>

            <tr>
              <th style="width:20%">Feeding<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:15%">Exercise<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:20%">Grooming<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:20%">Medicines and Poisons<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:30%">Spaying, Neutering and Vaccinations<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>
<tr>
<td>Establish feeding schedule</td>
<td>Aim for 5 minutes of exercise twice a day per each month of age. Then, 20 minutes twice a day at 4 months.</td>
<td>Establish regular brushing to the dog’s coat.</td>
<td>When giving pills for medication, holding the dog's head pointing partially upward can help prevent spills.</td>
  <td>  Should consult with a veterinarian to know what the best age to spay or neuter.</td>

<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>
<tr>
<td>3 - 4 meals per day</td>
<td>Avoid forced exercise</td>
<td></td>
<td>When giving liquid for medication insert the tip of the syringe at the back teeth on either side.</td>
<td>Keep the dog inside and away from other animals during the recovery period.</td>
<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>
<tr>
<td>Should be fed a high-quality puppy food</td>
<td>Exercise may vary based on breed, size, and age.</td>
<td>Bathed every two months </td>
<td>Never give your dog medication that has not been prescribed by a veterinarian.</td>
<td>  Check the incision every day to make sure it’s healing properly.</td>
<td style="display:none;">67157725,73299305</td>
</tr>
<tr>
<td>Limit table food “people food”</td>
<td>Consistency of exercise is important.</td>
<td></td>
<td></td>
<td>Eight weeks old is mostly the age to start the vaccination. </td>
<td style="display:none;">5523749,34082255</td>
</tr>
<tr>
<td>Provide clean and fresh water all times</td>
<td>Teach basic commands: sit, stay, down, come and heel.</td>
<td></td>
<td></td>
<td>Avoid Skipping Vaccination</td>
<td style="display:none;">5523749,34082255</td>
</tr>
        </tbody>
</table>
      </div>
    </div>

  </body>
  </html>

</div>
 
</section>

<section id="adult" style="width: 70%; font-size: 16px; margin-bottom: 20px;">
<h2>Adult</h2>
  <link href="css/style.css" rel="stylesheet">
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
  zebraRows('tr:odd td', 'odd');
  
  $('tbody tr').hover(function(){
    $(this).find('td').addClass('hovered');
  }, function(){
    $(this).find('td').removeClass('hovered');
  });
  
  //default each row to visible
  $('tbody tr').addClass('visible');
  



  
  //overrides CSS display:none property
  //so only users w/ JS will see the
  //filter box
  $('#search').show();
  
  $('#filter').keyup(function(event) {
    //if esc is pressed or nothing is entered
    if (event.keyCode == 27 || $(this).val() == '') {
      //if esc is pressed we want to clear the value of search box
      $(this).val('');
      
      //we want each row to be visible because if nothing
      //is entered then all rows are matched.
      $('tbody tr').removeClass('visible').show().addClass('visible');
    }

    //if there is text, lets filter
    else {
      filter('tbody tr', $(this).val());
    }

    //reapply zebra rows
    $('.visible td').removeClass('odd');
    zebraRows('.visible:even td', 'odd');
  });
  
  //grab all header rows
  $('thead th').each(function(column) {
    $(this).addClass('sortable')
          .click(function(){
            var findSortKey = function($cell) {
              return $cell.find('.sort-key').text().toUpperCase() + ' ' + $cell.text().toUpperCase();
            };
            
            var sortDirection = $(this).is('.sorted-asc') ? -1 : 1;
            
            //step back up the tree and get the rows with data
            //for sorting
            var $rows   = $(this).parent()
                                .parent()
                                .parent()
                                .find('tbody tr')
                                .get();
            
            //loop through all the rows and find 
            $.each($rows, function(index, row) {
              row.sortKey = findSortKey($(row).children('td').eq(column));
            });
            
            //compare and sort the rows alphabetically
            $rows.sort(function(a, b) {
              if (a.sortKey < b.sortKey) return -sortDirection;
              if (a.sortKey > b.sortKey) return sortDirection;
              return 0;
            });
            
            //add the rows in the correct order to the bottom of the table
            $.each($rows, function(index, row) {
              $('tbody').append(row);
              row.sortKey = null;
            });
            
            //identify the column sort order
            $('th').removeClass('sorted-asc sorted-desc');
            var $sortHead = $('th').filter(':nth-child(' + (column + 1) + ')');
            sortDirection == 1 ? $sortHead.addClass('sorted-asc') : $sortHead.addClass('sorted-desc');
            
            //identify the column to be sorted by
            $('td').removeClass('sorted')
                  .filter(':nth-child(' + (column + 1) + ')')
                  .addClass('sorted');
            
            $('.visible td').removeClass('odd');
            zebraRows('.visible:even td', 'odd');
          });
  });
});


//used to apply alternating row styles
function zebraRows(selector, className)
{
  $(selector).removeClass(className)
              .addClass(className);
}

//filter results based on query
function filter(selector, query) {
  query = $.trim(query); //trim white space
  query = query.replace(/ /gi, '|'); //add OR for regex
  
  $(selector).each(function() {
    ($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
  });
}

$(".ErrorRow").dblclick(function(e){
  //alert($(this).find("td").eq(1).html());
  alert($(this).find("td:last").html());
})
  </script> -->

  </head>
  <body id="index" TOPMARGIN=0 LEFTMARGIN=0 >
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=100%>
          <thead>
            <tr>
              <th style="width:20%">Feeding<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:15%">Exercise <!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:20%">Grooming<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:20%">Medicines and Poisons<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:30%">Spaying, Neutering and Vaccinations<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>
<tr class="ErrorRow">
<td>Establish feeding schedule</td>
<td>Exercise may vary based on breed or breed mix, sex, age and level of health (Consult your veterinarian before beginning an exercise program for your dog.)</td>
<td>Before bathing, comb or cut out all mats from the coat.</td>
<td>When giving pills for medication, holding the dog's head pointing partially upward can help prevent spills.</td>
  <td>  Eight weeks old is mostly the age to start the vaccination. </td>

<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>
<tr class=ErrorRow>
<td>2 meals per day</td>
<td>Activities for Exercising Your Dog: Walking or jogging, Fetch, Playing with other pets, Running off leash, Swimming (great for arthritic dogs), Tricks for low-calorie treats </td>
<td>Bath your dog atleast once every three months. (It may vary if the dog spends a lot of time outdoors and has a skin problems)</td>
<td>When giving liquid for medication insert the tip of the syringe at the back teeth on either side.</td>
<td> Avoid Skipping Vaccination</td>
<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>

<tr class=ErrorRow>
<td>Premium-quality dry food (can be mixed with water, broth, or canned food)</td>
<td></td>
<td>Brush the dog’s teeth regularly.</td>
<td>Never give your dog medication that has not been prescribed by a veterinarian.</td>
<td></td>
<td style="display:none;">67157725,73299305</td>
</tr>

<tr class=ErrorRow>
<td> Dog owner may give cottage cheese, cooked egg or fruits </td>
<td></td>
<td>Establish regular ear checks. (Don’t clean the ear frequently.</td>
<td></td>
<td></td>
<td style="display:none;">5523749,34082255</td>
</tr>
<tr class=ErrorRow>
<td>Provide clean and fresh water all times</td>
<td>Teach basic commands: sit, stay, down, come and heel.</td>
<td></td>
<td></td>
<td>Avoid Skipping Vaccination</td>
<td style="display:none;">5523749,34082255</td>
</tr>
        </tbody>
</table>
      </div>
    </div>
    
  </body>

    <script src="js/main.js"></script>
  </body>
  </html>

</section>

<section id="conditions" style="width: 70%; font-size: 16px; margin-bottom: 20px;">
<h1 style="font-size: 30px;"><b>Based on Conditions</b></h1> 
  <link href="conditions/css/style.css" rel="stylesheet">
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
  zebraRows('tr:odd td', 'odd');
  
  $('tbody tr').hover(function(){
    $(this).find('td').addClass('hovered');
  }, function(){
    $(this).find('td').removeClass('hovered');
  });
  
  //default each row to visible
  $('tbody tr').addClass('visible');
  



  
  //overrides CSS display:none property
  //so only users w/ JS will see the
  //filter box
  $('#search').show();
  
  $('#filter').keyup(function(event) {
    //if esc is pressed or nothing is entered
    if (event.keyCode == 27 || $(this).val() == '') {
      //if esc is pressed we want to clear the value of search box
      $(this).val('');
      
      //we want each row to be visible because if nothing
      //is entered then all rows are matched.
      $('tbody tr').removeClass('visible').show().addClass('visible');
    }

    //if there is text, lets filter
    else {
      filter('tbody tr', $(this).val());
    }

    //reapply zebra rows
    $('.visible td').removeClass('odd');
    zebraRows('.visible:even td', 'odd');
  });
  
  //grab all header rows
  $('thead th').each(function(column) {
    $(this).addClass('sortable')
          .click(function(){
            var findSortKey = function($cell) {
              return $cell.find('.sort-key').text().toUpperCase() + ' ' + $cell.text().toUpperCase();
            };
            
            var sortDirection = $(this).is('.sorted-asc') ? -1 : 1;
            
            //step back up the tree and get the rows with data
            //for sorting
            var $rows   = $(this).parent()
                                .parent()
                                .parent()
                                .find('tbody tr')
                                .get();
            
            //loop through all the rows and find 
            $.each($rows, function(index, row) {
              row.sortKey = findSortKey($(row).children('td').eq(column));
            });
            
            //compare and sort the rows alphabetically
            $rows.sort(function(a, b) {
              if (a.sortKey < b.sortKey) return -sortDirection;
              if (a.sortKey > b.sortKey) return sortDirection;
              return 0;
            });
            
            //add the rows in the correct order to the bottom of the table
            $.each($rows, function(index, row) {
              $('tbody').append(row);
              row.sortKey = null;
            });
            
            //identify the column sort order
            $('th').removeClass('sorted-asc sorted-desc');
            var $sortHead = $('th').filter(':nth-child(' + (column + 1) + ')');
            sortDirection == 1 ? $sortHead.addClass('sorted-asc') : $sortHead.addClass('sorted-desc');
            
            //identify the column to be sorted by
            $('td').removeClass('sorted')
                  .filter(':nth-child(' + (column + 1) + ')')
                  .addClass('sorted');
            
            $('.visible td').removeClass('odd');
            zebraRows('.visible:even td', 'odd');
          });
  });
});


//used to apply alternating row styles
function zebraRows(selector, className)
{
  $(selector).removeClass(className)
              .addClass(className);
}

//filter results based on query
function filter(selector, query) {
  query = $.trim(query); //trim white space
  query = query.replace(/ /gi, '|'); //add OR for regex
  
  $(selector).each(function() {
    ($(this).text().search(new RegExp(query, "i")) < 0) ? $(this).hide().removeClass('visible') : $(this).show().addClass('visible');
  });
}

$(".ErrorRow").dblclick(function(e){
  //alert($(this).find("td").eq(1).html());
  alert($(this).find("td:last").html());
})
  </script>
 -->
  </head>
  <body id="index" TOPMARGIN=0 LEFTMARGIN=0>
      
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=100%>
          <thead>
            <tr>
              <th style="width:20%">Conditions<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:50%">Tips</th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>
<tr class="ErrorRow">
<td>Allergies - Flea</td>
<td>1. Break the cycle. Wash your bedding, pet's bedding and rugs with detergent and warm water. Then, vacuum your carpets, rugs, chair and sofa cushions to remove fleas, flea eggs, and larvae.
<br><br> 2. Avoid irritants. Don’t use flea shampoo or other topical flea products without talking to your pet’s veterinarian first. <br><br>3. Stay in touch. Get regular checkups to your pet's veterinarian.</td>


<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>

<tr class=ErrorRow>
<td>Allergies - Food</td>
<td>1. Avoid table food. The most common allergens are beef, dairy, wheat, egg, chicken, lamb, soy, pork, rabbit, and fish.</td>

<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>

<tr class=ErrorRow>
<td>Anal Sac Disease</td>
<td>1. Prevention. Put your dog on a healthy diet and make sure they get plenty of exercise. <br><br> 2. Contact Vet. The treatment will be given based on the severity of the disease. </td>

<td style="display:none;">67157725,73299305</td>
</tr>

<tr class=ErrorRow>
<td> Bladder Stones</td>
<td>1. Check for genitals. Look for redness, swelling, signs of scratching or biting. <br><br>2. Water Intake. Keep your dog well-hydrated. If not readily drink water offer them a hig moisture diet (canned food). <br><br>3. Don't use DIY treatment. Do not give treatment to your dog unless a vet prescribe or consult it. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Bone Cancer</td>
<td>1. Give them regular exercise. It keeps bones strong, as well as the tissues that surround the bones.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Bronchitis</td>
<td>1. Give your dog a rest, warmth and proper hygiene.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Cushing's Disease</td>
<td>1. Stay in touch. Must have an adequate monitoring and follow-up checkups.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Cancer</td>
<td>1. Healthy weight. By getting a good-quality food and activity helps your dog to have a healthy body weight. <br><br> 2. Make an appointment. Make sure to schedule an appointment as soon as possible. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Canine Herpes - Adult Dogs</td>
<td>1. Preventing. Isolate pregnant dogs for three weeks before and after delivery. - Isolate puppies for three weeks after birth. - Keep the puppies warm (above 95 degrees) </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Canine Herpes - Puppies</td>
<td>1. Best protection. People in contact with dogs during this time should wash their hands thoroughly and often </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Deafness</td>
<td>1. Keep Track. Put a bell on the collar and a tag stating ''Deaf'' including your information.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Demodetic mange</td>
<td>1. Keep pets away from any other animals suspected to have mange, including avoiding public dog parks or similar areas that may foster contagious outbreaks.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Ear Infection</td>
<td>1. Keep the Ears Dry. Cleansing and drying the ears every five to ten days would be the best for prevention of this infection.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Ehrlichiosis</td>
<td>1. • Keep your grass nice and short and keep the dog away from bushy areas. <br><br> 2. Ensure your dog is not exposed to ticks. <br><br> 3. Check your dog regularly for ticks by running your fingers through their coat, on the skin. Pay attention to the head, neck, ears, chest, between their toes and around their mouths and gums. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Fecal impaction/constipation</td>
<td>1. Give the dog regular exercise. 2. Avoid sudden change of diet. <br><br> 3. Be aware of what the dog try to ingest: toys, gravel, plants, dirt and bones. <br><br> 4. Consult to the vet what diet have a high in fiber. </td>

<td style="display:none;">5523749,34082255</td>
</tr>
<tr class=ErrorRow>
<td>Folliculitis</td>
<td>1. Stay up to date on flea and tick prevention. Treat your dog for fleas, ticks, mites, and anything that could potentially infect their skin. <br><br> 2. When you notice any skin irritation it is the best to get them to the vet.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>
        </tbody>
</table>
      </div>
    </div>
  </body>
  </html>

</section>

<section id="smallbreed" style="width: 70%; font-size: 16px; margin-bottom: 20px;">
<h1 style="font-size: 30px;"><b>Based on Size</b></h1>
<h2>Small Breed</h2>
  <link href="conditions/css/style.css" rel="stylesheet">
  <body id="index" TOPMARGIN=0 LEFTMARGIN=0>
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=70%>
          <thead>
            <tr>
              <th style="width:10%">Breed<img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"></th>
              <th style="width:15%">Photo</th>
              <th style="width:30%">Tips</th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>
<tr class="ErrorRow">
<td></td>
<td></td>


<td style="display:none;">67157725,73299305</td>


<tr class="ErrorRow">
    <td style="text-align: center;">Shihtzu</td>
    <td><img src="images/shihtzu.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>
  


<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>

<tr class="ErrorRow">

    <td style="text-align: center;">Chihuahua</td>
    <td><img src="images/chihuahua.jpg" width="200" height="222"></td>
    <td>1. Give High-Quality Food.
  <br>2. Avoid Overfeeding Your Chihuahua. <br>3. Feed Him Multiple Times a Day.<br>4. Observe Him for Dietary-Related Problems.<br>5. Avoid table food.<br>6. Brush based on the type of coat.<br>7. Use lukewarm water when bathing <br>8. Check the body for abnormalities.<br>9. Consult your veterinarian before you deworm your Chihuahua.<br>10. Consult your veterinarian to know how to vaccinate your Chihuahua.<br>11. Train your Chihuahua or enroll to a puppy class. <br>12. Avoid strenuous exercises.</td>


<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>
<tr class="ErrorRow">

    <td style="text-align: center;">Pomeranian</td>
    <td><img src="images/pomeranian.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>


<td style="display:none;">67157725,73299305</td>
</tr>

<tr class="ErrorRow">

    <td style="text-align: center;">Pug</td>  
    <td><img src="images/pug.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>


<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

    <td style="text-align: center;">Poodle</td>
    <td><img src="images/poodle.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>


<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

    <td style="text-align: center;">Maltese</td>
    <td><img src="images/maltese.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>


<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

    <td style="text-align: center;">Pekingese</td>
    <td><img src="images/pekingese.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>



<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

    <td style="text-align: center;">Yorkshire Terrier</td>
    <td><img src="images/yorkie.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>


<td style="display:none;">67157725,73299305</td>
</tr>

    <td style="text-align: center;">French Bulldog</td>
    <td><img src="images/french.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>



<td style="display:none;">67157725,73299305</td>
 </tr>
    <td style="text-align: center;">Corgi</td>
    <td><img src="images/corgi.jpg" width="200" height="222"></td>
    <td>1. Offer all-natural food.
  <br>2. Offer spring or filtered water. <br>3. Supplements, when needed.<br>4. On-time grooming.<br>5. Use quality coat products.<br>6. Put Care into your Shih Tzu’s dental hygiene.<br>7. Use a harness, not a collar when on leash. <br>8. Aim to meet exercise requirements.<br>9. Keep an eye on your Shih Tzu’s breathing.<br>10. Use flea, tick, and heartworm prevention, while avoiding as many chemicals as possible.<br>11. Bring your Shih Tzu to veterinary wellness checks. <br>12. Address separation anxiety issues.<br>13. Teach proper hierarchy.</td>

<td style="display:none;">67157725,73299305</td>
</tr>
        </tbody>
</table>
      </div>
    </div>
  </body>

    <script src="js/main.js"></script>
  </body>
  </html>

</section>
<section id="largebreed"style="width: 70%; font-size: 16px; margin-bottom: 20px;">
<h2>Large Breed</h2>
  <body id="index" TOPMARGIN=0 LEFTMARGIN=0>
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=100%>
          <thead>
            <tr>
              <th style="width:10%">Breed<img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"></th>
              <th style="width:5%">Photo</th>
              <th style="width:20%">Tips</th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>

<tr class="ErrorRow" >
    
    <td style="text-align: center;">Rottweiler</td>
    <td><img src="images/rottweiler.jpg" width="200" height="222"></td>
    <td>1. Regular Grooming of your Rottweiler.<br>
2. Feed puppy a nutritious diet. <br>
3. House train the puppy right away. <br> 
4. Start socializing the puppy at an early age. <br>
5. Make sure the rottweiler puppy is comfortable. <br>
6. Supervise interactions with other dogs.  <br>
7. Get the puppy lots of exercise.  </td>
  


<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>

<tr class="ErrorRow">

    
    <td style="text-align: center;">Great Dane</td>
    <td><img src="images/greatdane.jpg" width="200" height="222"></td>
    <td>1. Establish a Brushing Routine <br>
2. Provide Lots of Attention  <br>
3. Go for a short and Simple Walks  <br>
4. Pay Close Attention to Their Eyes  <br>
5. Invest in a Large Dog Bed  <br>
6. Use a Ramp to Minimize Jumping <br>
7. Be Aware of Bloat  <br>
8. Schedule Routine Check-Ups </td>


<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>

<tr class="ErrorRow">

   
    <td style="text-align: center;">Boxer</td>
    <td><img src="images/boxer.jpg" width="200" height="222"></td>
    <td>1. Go as high quality as you can afford. <br>               
2. Take care in regard to the water you give to your Boxer. <br>              
3. Boxers should be walked at least twice per day. For pups, this should be for at least 20 minutes, adults do good with 30. But don't stop there.<br>                
4. You'll need to keep up on eye, paw and nose care, or things can take a downward turn rather quickly.  <br>             </td>



<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

   
    <td style="text-align: center;">Collie</td> 
    <td><img src="images/collie.jpg" width="200" height="222"></td>

    <td>1. Housetraining your Border Collie <br>
2. Trick Training the dog <br>
3. Crate Training <br></td>


<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

   
    <td style="text-align: center;">Standard Poodle</td> 
    <td><img src="images/standard.jpg" width="200" height="222"></td>
    <td>1.  Offer a high-quality food <br>  
2. Choose healthy snacks  <br>  
3. Assess the Water that Your Poodle Drinks.  <br>  
4. Choose The Right Bowl  <br>  
5. Making sure that the Poodle receives enough exercise is vital to overall good health and is a step that can help increase life span. <br>    
6. Do annual vet visits </td>


<td style="display:none;">67157725,73299305</td>
</tr>
<tr class="ErrorRow">

    
    <td style="text-align: center;">German Shepherd</td> 
    <td><img src="images/german.jpg" width="200" height="222"></td>
    <td>1. Make sure your German shepherd is getting proper feeding.  <br>
2. Taking Care of Your German Shepherd's Health <br>
3. Keeping Your German Shepherd Physically Active <br>
4. Training and Socializing Your German Shepherd <br> </td>



<td style="display:none;">67157725,73299305</td>
</tr>

        </tbody>
</table>
      </div>
    </div>
  </body>

    <script src="js/main.js"></script>
  </body>
  </html>

</section>




<body>
<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat');

* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
body {
}

.accordion {
  width: 60%;
  max-width: 500px;
  margin: 2rem auto;
  margin-top: 250px;
  margin-left: 1100px;
}
.accordion-item {
  background-color: #fff;
  color: #292f33;
  margin: 1rem 0;
  border-radius: 0.3rem;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,0.25);
}
.accordion-item-header {
  padding: 0.5rem 3rem 0.5rem 1rem;
  min-height: 3.5rem;
  line-height: 1.25rem;
  font-weight: bold;
  display: flex;
  font-size: 17px;
  align-items: center;
  position: relative;
  cursor: pointer;
}
.accordion-item-header::after {
  content: "\002B";
  font-size: 2rem;
  position: absolute;
  right: 1rem;
}
.accordion-item-header.active::after {
  content: "\2212";
}
.accordion-item-body {
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
.accordion-item-body-content {
  text-align: justify;
  padding: 1rem;
  line-height: 1.5rem;
  font-family: sans-serif;
  border-top: 1px solid;
  font-size: 16px;
  border-image: linear-gradient(to right, transparent, #34495e, transparent) 1;
}

@media(max-width:767px) {
  html {
    font-size: 14px;
  }
}
</style>

  <div class="accordion" style=" width: 60%; margin-top: -565%; margin-left: 950px;">
    <h1 style="text-align: center;">Frequently Ask Questions</h1>
    <div class="accordion-item">
      <div class="accordion-item-header">
      Are some breeds more "kid friendly" than others? Which ones?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
           The highly trainable breeds are great with kids because they’re usually eager to please and are usually more family oriented rather than wanting to attach themselves to one person. Look at the sporting group. These are dogs that were bred to work side by side with humans, taking direction from their owner. They also are some of the most popular family dogs -- your golden retrievers, your Labrador retrievers. Some of the herding breeds, such as German shepherds and collies, also are highly trainable.
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        My child has allergies. Are there truly hypoallergenic dogs?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          There are no 100% hypoallergenic dogs, but there are dogs that shed less. Those dogs produce less dander, so people with allergies tend to tolerate them better. Those are breeds like the Bichon, the Portuguese water dog, the Kerry blue terrier, the Maltese, poodles. <br><br>I’d also suggest you spend time with the individual dog you’re thinking about bringing home. Even in the hypoallergenic breeds, you may have a sensitivity to one particular dog more than another. You also can be sensitive to their saliva, so let them lick your hand or kiss your face just to be sure that doesn’t cause a reaction.
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        Why dog care is important?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
        Dog care is the thing you should know to nourish your dog, make them happy and stay healthy. This should cover since well-selected quality dog food, taking care basic dog’s health, dental hygiene, and grooming for healthy skin & coat. You should also regularly take him to visit a vet to keep up the good health condition.

        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        What are the basic needs of a dog?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
        Like humans, Dogs also need a quality food with complete and balanced for their health development, water, good care, safe, explore the world and socialize especially bonding and interaction with dog’s owner. Therefore, dog need is not just a physical part, but also a mental part to make them live happily longer as well.
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        What makes a dog happy?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          To make your dog happy, you can take him out for a walk, explore the world, play and make them fun with the new experience. 
        </div>
      </div>
    </div>
<div class="accordion-item">
      <div class="accordion-item-header">
        What is the Asian Kennel Club Union of the Philippines, Inc. (AKCUPI)?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
         AKCUPI is a duly recognized by the Philippine government as a legitimate dog registering organization. Registered with the Securities & Exchange Commission and Department of Trade and Industry in 2008, AKCUPI is here to serve the needs of the dog owning public. Our goal is to offer fast, reliable and affordable dog registration services. This is main the reason why AKCUPI was established, with you the dog owner in mind. Our friendly staff is there to assist you to make registering your dog an easy & pleasurable experience.
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        What should I do if I see animal cruelty or neglect?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          The first step is to immediately tell the offender to stop the abuse, as it may save the animal’s life. However, animal cruelty is a crime and MUST be reported to your barangay officials and/or police hotline. Otherwise, offenders will only keep abusing and mistreating animals. 
        </div>
      </div>
    </div>
<div class="accordion-item">
      <div class="accordion-item-header">
        What do I do about vet malpractice?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          A comprehensive investigation, including a necropsy if your pet has died, will determine whether there malpractice occurred in the treatment of your pet. PAWS only has one vet attending to 200+ shelter animals and spay/neuter clients, therefore we cannot offer this service. If you wish to press charges, please consult with a private lawyer as our volunteer lawyers already have their hands full with ongoing animal cruelty cases.
        </div>
      </div>
    </div>
    <div class="accordion-item">
      <div class="accordion-item-header">
        Why is my dog panting?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          It's because they don’t sweat like humans, excessive heat is lost as air moves through your dog’s airways. While normal respiration in dogs can be up to 30-40 breaths per minute, a panting dog can exchange air up to 300-400 times per minute.

<br><br>Some causes of abnormal panting can include stress, pain, endocrine diseases, heatstroke, and heart or respiratory disorders such as tracheal collapse. If your dog doesn’t need to chill out in a warm environment, bring abnormal panting to your veterinarian’s attention.
        </div>
      </div>
    </div>
<div class="accordion-item">
      <div class="accordion-item-header">
        Why does my dog eat poop?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          Some dogs eat stool (coprophagy) because they are stressed, bored, like the attention they receive, even negative attention, or to avoid punishment.

<br><br> Some dogs eat poop just because they like the taste! Discourage the opportunity to eat stool by picking up poop immediately
        </div>
      </div>
    </div>

    <div class="accordion-item">
      <div class="accordion-item-header">
        Why Do Dogs Sniff Each Other’s Butts?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          Anal glands, which secrete a foul-smelling liquid, are used for scent marking and identification. Because of their complex olfactory system which includes a component called Jacobsen’s organ in addition to their nose, they can also process scents that have no detectable odor like pheromones.

<br><br>A butt sniff is the equivalent of a human handshake enabling dogs to recognize each other as individuals and identify sex, age, variabilities in health and social order.
        </div>
      </div>
    </div>
<div class="accordion-item">
      <div class="accordion-item-header">
        Why Do Dogs Walk in Circles Before Lying Down?
      </div>
      <div class="accordion-item-body">
        <div class="accordion-item-body-content">
          Back in the days before fluffy pillows, dogs had to make their own bed. By flattening the grasses and brush for themselves and their young, they made a comfortable nest and drove out critters such as snakes. Scratching imparts a scent to their bed expressed by the small glands in the feet, further defining the space as their own.

<br><br>While we’ve domesticated pets, some of these instinctual behaviors and functional anatomic features are part of what makes dogs such fascinating creatures! 
        </div>
      </div>
    </div>

  </div>
<script>
  const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

accordionItemHeaders.forEach(accordionItemHeader => {
  accordionItemHeader.addEventListener("click", event => {



    accordionItemHeader.classList.toggle("active");
    const accordionItemBody = accordionItemHeader.nextElementSibling;
    if(accordionItemHeader.classList.contains("active")) {
      accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    }
    else {
      accordionItemBody.style.maxHeight = 0;
    }

  });
});
</script>
</body>
</html>
  </main>

  
<script>
  const toggle = document.querySelector(".toggle");
const nav_bar = document.querySelector(".table-of-contents");
const list = document.querySelector(".table__list");

toggle.addEventListener("click", ()=>{
  if (toggle.textContent === "hide") {
    toggle.textContent = "show";
  }
  else {
    toggle.textContent = "hide";
  }
  
  list.classList.toggle("list-invisible");
  nav_bar.classList.toggle("table-narrow");
});
</script>
    
  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>

