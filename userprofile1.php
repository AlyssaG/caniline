
<?php 
  include('functions.php');
  if (!isLoggedIn()) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Caniline</title>
  
  <meta content="" name="description">
  <meta content="" name="keywords">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

  <!-- =======================================================
  * Template Name: Squadfree - v4.9.1
  * Template URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<style>
body{
    margin-top:20px;
    background:#f5f5f5;
}


/* ===========
   Profile
 =============*/
.card-box {
  padding: 20px;
  box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.06), 0 2px 0 0 rgba(0, 0, 0, 0.02);
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  background-clip: padding-box;
  margin-bottom: 20px;
  background-color: #ffffff;
}
.header-title {
  text-transform: uppercase;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.04em;
  line-height: 16px;
  margin-bottom: 8px;
}
.social-links li a {
  -webkit-border-radius: 50%;
  background: #EFF0F4;
  border-radius: 50%;
  color: #7A7676;
  display: inline-block;
  height: 30px;
  line-height: 30px;
  text-align: center;
  width: 30px;
}

/* ===========
   Gallery
 =============*/
.portfolioFilter a {
  -moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -moz-transition: all 0.3s ease-out;
  -ms-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -webkit-transition: all 0.3s ease-out;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  color: #333333;
  padding: 5px 10px;
  display: inline-block;
  transition: all 0.3s ease-out;
}
.portfolioFilter a:hover {
  background-color: #228bdf;
  color: #ffffff;
}
.portfolioFilter a.current {
  background-color: #228bdf;
  color: #ffffff;
}
.thumb {
  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  margin-top: 30px;
  padding-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  width: 100%;
}
.thumb-img {
  border-radius: 2px;
  overflow: hidden;
  width: 100%;
}
.gal-detail h4 {
  margin: 16px auto 10px auto;
  width: 80%;
  white-space: nowrap;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
}
.gal-detail .ga-border {
  height: 3px;
  width: 40px;
  background-color: #228bdf;
  margin: 10px auto;
}

.tabs-vertical-env .tab-content {
  background: #ffffff;
  display: table-cell;
  margin-bottom: 30px;
  padding: 30px;
  vertical-align: top;
}
.tabs-vertical-env .nav.tabs-vertical {
  display: table-cell;
  min-width: 120px;
  vertical-align: top;
  width: 150px;
}
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  background-color: #ffffff;
  border: 0;
}
.tabs-vertical-env .nav.tabs-vertical li > a {
  color: #333333;
  text-align: center;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  white-space: nowrap;
}
.nav.nav-tabs > li.active > a {
  background-color: #ffffff;
  border: 0;
}
.nav.nav-tabs > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  font-weight: 500;
  padding-left: 20px;
  padding-right: 20px;
  font-family: 'Roboto', sans-serif;
}
.nav.nav-tabs > li > a:hover {
  color: #228bdf !important;
}
.nav.tabs-vertical > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  padding-left: 20px;
  padding-right: 20px;
}
.nav.tabs-vertical > li > a:hover {
  color: #228bdf !important;
}
.tab-content {
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
  color: #777777;
}
.nav.nav-tabs > li:last-of-type a {
  margin-right: 0px;
}
.nav.nav-tabs {
  border-bottom: 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}
.navtab-custom li {
  margin-bottom: -2px;
}
.navtab-custom li a {
  border-top: 2px solid transparent !important;
}
.navtab-custom li.active a {
  border-top: 2px solid #228bdf !important;
}
.nav-tab-left.navtab-custom li a {
  border: none !important;
  border-left: 2px solid transparent !important;
}
.nav-tab-left.navtab-custom li.active a {
  border-left: 2px solid #228bdf !important;
}
.nav-tab-right.navtab-custom li a {
  border: none !important;
  border-right: 2px solid transparent !important;
}
.nav-tab-right.navtab-custom li.active a {
  border-right: 2px solid #228bdf !important;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus,
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  border: none;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:focus,
.nav-tabs > li.active > a:hover,
.tabs-vertical > li.active > a,
.tabs-vertical > li.active > a:focus,
.tabs-vertical > li.active > a:hover {
  color: #228bdf !important;
}

.nav.nav-tabs + .tab-content {
    background: #ffffff;
    margin-bottom: 20px;
    padding: 20px;
}
.progress.progress-sm .progress-bar {
    font-size: 8px;
    line-height: 5px;
}
</style>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-image: url(images/full.png)">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1 class="text-light"><a href="index.php"><span>Caniline</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

     <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="index.php">Home</a></li>
          <li><a class="nav-link scrollto" href="userprofile.php">User Profile</a></li>
          <li><a class="nav-link scrollto" href="symptomcheck.php">Symptom Check</a></li>
          <li><a class="nav-link scrollto" href="vetlocation.php">Vet Location</a></li>
          <li class="dropdown"><a href="caretips.php"><span>Care Tips</span></i></a>
          </li>
          <li class="dropdown"><a href="index.php?logout='1'"><span>Log out</span> <i class="bi bi-chevron-down"></i></a>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <ol style="float: left;">
            <li><a href="index.php">Home</a></li>
            <li><a href="userprofile.php">User Profile</a></li>
          </ol>
        </div>

      </div>
    </section><!-- Breadcrumbs Section -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4">
            <div class="text-center card-box">
                <div class="member-card">
                    <div class="thumb-xl member-thumb m-b-10 center-block">
                      <div class="frame">
<img src="images/client.jpg" class="img-circle img-thumbnail" alt="profile-image" style="width: 300px; height: 200px;">
                      </div>

                    <div class="">
                      <div style="padding: 15px;">
                        <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['username']; ?></strong>
                          <small>
                            <!-- <i  style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>  -->
                            <br>
                          </small>
                        <?php endif ?>
                        
                      </div>
                    </div>
          
                  
                    <div style="margin-bottom: 10%;">
                    <button type="button" class="btn btn-danger btn-sm w-sm waves-effect m-t-10 waves-light" data-target="#editprofilemodal"data-toggle="modal"  style="background-color: #38b6ff; border-style: none; "><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Edit Profile</button>
                    </div>
                    <div class="text-left m-t-40" style="text-align: justify;">
                        <p class="text-muted font-13">Full Name : <span class="m-l-15"></span>
                          <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['fullname']; ?></strong>
                          <small>
                            <br>
                          </small>
                        <?php endif ?></p>
                        <p class="text-muted font-13">Mobile : <span class="m-l-15"></span>
                          <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['mobile']; ?></strong>
                          <small>
                            <br>
                          </small>
                        <?php endif ?></p>
                        <p class="text-muted font-13">Email : <span class="m-l-15"></span>
                        <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['email']; ?></strong>
                          <small>
                            <br>
                          </small>
                        <?php endif ?>
                    </p>
                        <p class="text-muted font-13">Address :<span class="m-l-15"></span>
                          <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['address']; ?></strong>
                          <small>
                            <br>
                          </small>
                        <?php endif ?></p>
                    </div>
                </div>
              </div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->

        <div class="col-md-8 col-lg-9">
            <div class="">
                <div class="">
                    <ul class="nav nav-tabs navtab-custom">
                        <li class="active">
                                <button class="btn-sm w-sm waves-effect m-t-10 waves-light" data-target="#studentaddmodal"data-toggle="modal"  style="background-color: #38b6ff; border-style: none; color: white;"><i class="fa fa-plus  "></i> Add Dog</button>
                                <br>
                                <br>
                        </li>
                    </ul>
                         <div class="card">
                <div class="card-body">

                <?php
                $connection = mysqli_connect("localhost","root","");
                $db = mysqli_select_db($connection, 'multi_login');

                $query = "SELECT * FROM pet";
                $query_run = mysqli_query($connection, $query);
            ?>
                    <table id="datatableid" class="table table-bordered table-light">
                        <thead>
                            <tr>
                                <th style="display:none;"> ID</th>
                                <th scope="col">Image</th>
                                <th scope="col">Dog Name</th>
                                <th scope="col">Birthdate </th>
                                <th scope="col"> Gender </th>
                                <th scope="col"> Breed </th>
                                <th> EDIT &nbsp; &nbsp;&nbsp; &nbsp;  DELETE </th>
                            </tr>
                        </thead>
                        <?php
                if($query_run)
                {
                    foreach($query_run as $row)
                    {
            ?>
                        <tbody>
                            <tr>
                                <td style="display:none;"> <?php echo $row['petID']; ?> </td>
                                <td><img src="images/<?php echo $row['dogImage'];?>" class="img-circle img-thumbnail" alt="profile-image" style="width: 200px; height: 150px;"></td>
                                <td> <?php echo $row['dogName']; ?> </td>
                                <td> <?php echo $row['birthDate']; ?> </td>
                                <td> <?php echo $row['gender']; ?> </td>
                                <td> <?php echo $row['breed']; ?> </td>
                                <!-- <td>
                                    <button type="button" class="btn btn-info viewbtn"> VIEW </button>
                                </td> -->
                                <td>
                                    <button type="button" class="btn btn-success editbtn" style="background-color: #38b6ff"> <i class="fa fa-edit"></i> </button>
                                    &nbsp; &nbsp;&nbsp; &nbsp;
                                    <button type="button" class="btn btn-danger deletebtn" style="background-color: #38b6ff" > <i class="fa fa-trash-o"></i> </button>
                                </td>
                            </tr>
                        </tbody>
                        <?php           
                    }
                }
                else 
                {
                    echo "No Record Found";
                }
            ?>
                    </table>
                </div>
            </div>


        </div>
    </div>

          </main><!-- End #main -->

  
  <!-- Modal -->
    <div class="modal fade" id="studentaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Dog</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="insertcode.php" method="POST">

                    <div class="modal-body">
                      <div class="form-group">
                            <label>  Image </label>
                            <input type="file" name="dogImage" class="form-control" placeholder="Enter Dog's Name">
                        </div>
                        <div class="form-group">
                            <label> Dog Name </label>
                            <input type="text" name="dogName" class="form-control" placeholder="Enter Dog's Name">
                        </div>

                        <div class="form-group">
                            <label> Birth Date </label>
                            <input type="Date" name="birthDate" class="form-control" placeholder="">
                        </div>

                        <div class="form-group">
                            <label> Gender </label>
                            <input type="text" name="gender" class="form-control" placeholder="Enter Gender">
                        </div>

                        <div class="form-group">
                            <label> Breed </label>
                            <input type="text" name="breed" class="form-control" placeholder="Enter Breed">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="insertdata" class="btn btn-primary">Save Profile</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

 <!-- EDIT POP UP FORM (Bootstrap MODAL) -->
    <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Edit Dog Profile </h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="updatecode.php" method="POST">

                    <div class="modal-body">
                       <div class="form-group">
                            <label>  Image </label>
                            <input type="file" name="dogImage" class="form-control" placeholder="Enter Dog's Name">
                        </div>
                        <div class="form-group">
                            <label> Dog Name </label>
                            <input type="text" name="dogName" class="form-control" placeholder="Enter Dog's Name">
                        </div>

                        <div class="form-group">
                            <label> Birth Date </label>
                            <input type="Date" name="birthDate" class="form-control" placeholder="">
                        </div>

                        <div class="form-group">
                            <label> Gender </label>
                            <input type="text" name="gender" class="form-control" placeholder="Enter Gender">
                        </div>

                        <div class="form-group">
                            <label> Breed </label>
                            <input type="text" name="breed" class="form-control" placeholder="Enter Breed">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" name="updatedata" class="btn btn-primary">Update Profile</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

<!-- EDIT POP UP FORM (Bootstrap MODAL) -->
    <div class="modal fade" id="editprofilemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Edit Profile </h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="updateprofilecode.php" method="POST">

                    <div class="modal-body">
                       <div class="form-group">
                            <label>  Fullname </label>
                            <input type="text" name="fullname" class="form-control" placeholder="Enter Fullname">
                        </div>
                        <div class="form-group">
                            <label> Mobile </label>
                            <input type="text" name="mobile" class="form-control" placeholder="Enter Mobile">
                        </div>

                        <div class="form-group">
                            <label> Email </label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email Address">
                        </div>

                        <div class="form-group">
                            <label> Address </label>
                            <input type="text" name="address" class="form-control" placeholder="Enter Address">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" name="updatedata" class="btn btn-primary">Update Profile</button>
                    </div>
                </form>

            </div>
        </div>
    </div>



    <!-- DELETE POP UP FORM (Bootstrap MODAL) -->
    <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Delete Dog</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="deletecode.php" method="POST">

                    <div class="modal-body">

                        <input type="hidden" name="delete_id" id="delete_id">

                        <h4> Do you want to Delete this Profile ?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"> NO </button>
                        <button type="submit" name="deletedata" class="btn btn-primary"> YES </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
     </div>
                </form>

            </div>
        </div>
    </div>




    
  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function () {

            $('.viewbtn').on('click', function () {
                $('#viewmodal').modal('show');
                $.ajax({ //create an ajax request to display.php
                    type: "GET",
                    url: "display.php",
                    dataType: "html", //expect html to be returned                
                    success: function (response) {
                        $("#responsecontainer").html(response);
                        //alert(response);
                    }
                });
            });

        });
    </script>

<!-- 
    <script>
        $(document).ready(function () {

            $('#datatableid').DataTable({
                
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
               
            });

        });
    </script>
 -->
    <script>
        $(document).ready(function () {

            $('.deletebtn').on('click', function () {

                $('#deletemodal').modal('show');

                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function () {
                    return $(this).text();
                }).get();

                console.log(data);

                $('#delete_id').val(data[0]);

            });
        });
    </script>

    <script>
        $(document).ready(function () {

            $('.editbtn').on('click', function () {

                $('#editmodal').modal('show');

                $tr = $(this).closest('tr');

                var data = $tr.children("td").map(function () {
                    return $(this).text();
                }).get();

                console.log(data);

                $('#update_id').val(data[0]);
                $('#dogImage').val(data[1]);
                $('#dogName').val(data[2]);
                $('#birthDate').val(data[3]);
                $('#gender').val(data[4]);
                $('#breed').val(data[5]);
            });
        });
    </script>
<?php
    if(isset($_POST['inserdata']))
    { 
$dogImage=$_FILES['dogImage'];
 $imageFileType=pathinfo($target_file,PATHINFO_EXTENSION);
//Allow only JPG, JPEG, PNG & GIF etc formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
 $error[] = 'Sorry, only JPG, JPEG, PNG & GIF files are allowed';   
}
//Set image upload size 
    if ($_FILES["image"]["size"] > 1048576) {
   $error[] = 'Sorry, your image is too large. Upload less than 1 MB in size.';
}
if(!isset($error))
{
  //move image to the folder 
move_uploaded_file($file,$target_file); 
$result=mysqli_query($db,"INSERT INTO student(dogImage) VALUES('$dogIamge')"); 
if($result)
{
  $image_success=1;  
}
else 
{
  echo 'Something went wrong'; 
}
}
    }
if(isset($error)){ 

foreach ($error as $error) { 
  echo '<div class="message">'.$error.'</div><br>';   
}
}
  ?> 
  <div class="container">
  <?php if(isset($image_success))
    {
    echo '<div class="success">Image Uploaded successfully</div>'; 
    } ?>

</body>

</html>