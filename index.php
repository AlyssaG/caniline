<?php 
  include('functions.php');
  if (!isLoggedIn()) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Caniline</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Squadfree - v4.9.1
  * Template URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-transparent" style="background-image: url(images/full.png); background-size: cover;">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        
        <h1 class="text-light"><a href="index.php"><img src="images/caniline.png" alt="" class="img-fluid" ><span>Caniline</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="index.php">Home</a></li>
          <li><a class="nav-link scrollto" href="userprofile.php">User Profile</a></li>
          <li><a class="nav-link scrollto" href="symptomcheck1.php">Symptom Check</a></li>
          <li><a class="nav-link scrollto" href="vetlocation.php">Vet Location</a></li>
          <li class="dropdown"><a href="caretips.php"><span>Care Tips</span></i></a>
          </li>
          <li class="dropdown"><a href="index.php?logout='1'"><span>Log out</span> <i class="bi bi-chevron-down"></i></a>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Home Section ======= -->
  <section id="hero" style="background-image: url(images/userprofileBG.jpg); background-size: cover;">
    <div class="hero-container" data-aos="fade-up">
      <h1>Welcome to Caniline</h1>
      <br>
      <h2>Your DOG, Our CARE</h2>
      <a href="#about" class="btn-get-started scrollto"><i class="bx bx-chevrons-down"></i></a>
    </div>
  </section><!-- End Home -->

  <main id="main" >

    <!-- ======= Services Section ======= -->
    <section id="about" class="services" >
      <div class="container">

        <div class="section-title" data-aos="fade-in" data-aos-delay="100" >
          <h2>About Caniline</h2>
          
          <p style="text-align: center;">The main goal of Caniline is to maintain the users dogs' physical and mental wellbeing. </p>
          <br>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up">
              <div class="icon"><img src="images/userprofile.png" style='width:80px'></div>
              <br>
              <h4 class="title"><a href="">User Profile</a></h4>
              <p class="description" style="text-align: justify;">These sections include the user's profile and a list of the dogs they have submitted. The details and history of the symptom checker results are also shown on the dog's profile.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><img src="images/stepaw.png" style='width:100px; margin-top: -20px;'></div>
              <br>
              <h4 class="title" style="margin-top: -10px;"><a href="">Condition Analysis</a></h4>
              <p class="description" style="text-align: justify;">It assists dog owners figure out what to do when their dog exhibits symptoms and what conditions the dog might be dealing with.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><img src="images/caretips.png" style='width:50px'></div>
              <br>
              <h4 class="title"><a href="">Care Tips</a></h4>
              <p class="description" style="text-align: justify;">Owner is aware of how to properly care for their dog. In accordance with the system's findings and general advice, it will empower dog owners with knowledge about how to take care of their dogs.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
              <div class="icon"><img src="images/geolocation.png" style='width:50px'></div>
              <br>
              <h4 class="title"><a href="">Geolocation</a></h4>
              <p class="description" style="text-align: justify;">A city map with the locations of the closest veterinary clinics marked. The information of the location, including its contact details, address, and availability, is shown with a visual connection to a pinned location.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>