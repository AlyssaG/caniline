<?php error_reporting(0); ?> 
<?php 

include('dbconnection.php');
  include('functions.php');
  if (!isLoggedIn()) {
  $_SESSION['msg'] = "You must log in first";
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Caniline</title>
  
  <meta content="" name="description">
  <meta content="" name="keywords">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Squadfree - v4.9.1
  * Template URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<style>
body{
    margin-top:20px;
    background:#f5f5f5;
}
/* ===========
   Profile
 =============*/
.card-box {
  padding: 20px;
  box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.06), 0 2px 0 0 rgba(0, 0, 0, 0.02);
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  background-clip: padding-box;
  margin-bottom: 20px;
  background-color: #ffffff;
}
.header-title {
  text-transform: uppercase;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.04em;
  line-height: 16px;
  margin-bottom: 8px;
}
.social-links li a {
  -webkit-border-radius: 50%;
  background: #EFF0F4;
  border-radius: 50%;
  color: #7A7676;
  display: inline-block;
  height: 30px;
  line-height: 30px;
  text-align: center;
  width: 30px;
}

/* ===========
   Gallery
 =============*/
.portfolioFilter a {
  -moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -moz-transition: all 0.3s ease-out;
  -ms-transition: all 0.3s ease-out;
  -o-transition: all 0.3s ease-out;
  -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  -webkit-transition: all 0.3s ease-out;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
  color: #333333;
  padding: 5px 10px;
  display: inline-block;
  transition: all 0.3s ease-out;
}
.portfolioFilter a:hover {
  background-color: #228bdf;
  color: #ffffff;
}
.portfolioFilter a.current {
  background-color: #228bdf;
  color: #ffffff;
}
.thumb {
  background-color: #ffffff;
  border-radius: 3px;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
  margin-top: 30px;
  padding-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  width: 100%;
}
.thumb-img {
  border-radius: 2px;
  overflow: hidden;
  width: 100%;
}
.gal-detail h4 {
  margin: 16px auto 10px auto;
  width: 80%;
  white-space: nowrap;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
}
.gal-detail .ga-border {
  height: 3px;
  width: 40px;
  background-color: #228bdf;
  margin: 10px auto;
}




.tabs-vertical-env .tab-content {
  background: #ffffff;
  display: table-cell;
  margin-bottom: 30px;
  padding: 30px;
  vertical-align: top;
}
.tabs-vertical-env .nav.tabs-vertical {
  display: table-cell;
  min-width: 120px;
  vertical-align: top;
  width: 150px;
}
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  background-color: #ffffff;
  border: 0;
}
.tabs-vertical-env .nav.tabs-vertical li > a {
  color: #333333;
  text-align: center;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  white-space: nowrap;
}
.nav.nav-tabs > li.active > a {
  background-color: #ffffff;
  border: 0;
}
.nav.nav-tabs > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  font-weight: 500;
  padding-left: 20px;
  padding-right: 20px;
  font-family: 'Roboto', sans-serif;
}
.nav.nav-tabs > li > a:hover {
  color: #228bdf !important;
}
.nav.tabs-vertical > li > a {
  background-color: transparent;
  border-radius: 0;
  border: none;
  color: #333333 !important;
  cursor: pointer;
  line-height: 50px;
  padding-left: 20px;
  padding-right: 20px;
}
.nav.tabs-vertical > li > a:hover {
  color: #228bdf !important;
}
.tab-content {
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
  color: #777777;
}
.nav.nav-tabs > li:last-of-type a {
  margin-right: 0px;
}
.nav.nav-tabs {
  border-bottom: 0;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}
.navtab-custom li {
  margin-bottom: -2px;
}
.navtab-custom li a {
  border-top: 2px solid transparent !important;
}
.navtab-custom li.active a {
  border-top: 2px solid #228bdf !important;
}
.nav-tab-left.navtab-custom li a {
  border: none !important;
  border-left: 2px solid transparent !important;
}
.nav-tab-left.navtab-custom li.active a {
  border-left: 2px solid #228bdf !important;
}
.nav-tab-right.navtab-custom li a {
  border: none !important;
  border-right: 2px solid transparent !important;
}
.nav-tab-right.navtab-custom li.active a {
  border-right: 2px solid #228bdf !important;
}
.nav-tabs.nav-justified > .active > a,
.nav-tabs.nav-justified > .active > a:hover,
.nav-tabs.nav-justified > .active > a:focus,
.tabs-vertical-env .nav.tabs-vertical li.active > a {
  border: none;
}
.nav-tabs > li.active > a,
.nav-tabs > li.active > a:focus,
.nav-tabs > li.active > a:hover,
.tabs-vertical > li.active > a,
.tabs-vertical > li.active > a:focus,
.tabs-vertical > li.active > a:hover {
  color: #228bdf !important;
}

.nav.nav-tabs + .tab-content {
    background: #ffffff;
    margin-bottom: 20px;
    padding: 20px;
}
.progress.progress-sm .progress-bar {
    font-size: 8px;
    line-height: 5px;
}
</style>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top " style="background-image: url(images/full.png)">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1 class="text-light"><a href="index.php"><img src="images/caniline.png" alt="" class="img-fluid" ><span>Caniline</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

     <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="index.php">Home</a></li>
          <li><a class="nav-link scrollto" href="userprofile.php">User Profile</a></li>
          <li><a class="nav-link scrollto" href="symptomcheck1.php">Symptom Check</a></li>
          <li><a class="nav-link scrollto" href="vetlocation.php">Vet Location</a></li>
          <li class="dropdown"><a href="caretips.php"><span>Care Tips</span></i></a>
          </li>
          <li class="dropdown"><a href="index.php?logout='1'"><span>Log out</span> <i class="bi bi-chevron-down"></i></a>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">
<div class="result" style="width: 100%; height: 50%;">
  <script>//Breadcrumbs based on URL location
/*if ($('#siteBreadcrumb ol.breadcrumb')) {
  var here = location.href.replace(/(\?.*)$/, '').split('/').slice(3);

  var parts = [{
    "text": 'Home',
    "link": '/'
  }];

  for (var j = 0; j < here.length; j++) {
    var part = here[j];
    var pageName = part.toLowerCase();
    pageName = part.charAt(0).toUpperCase() + part.slice(1);
    var link = '/' + here.slice(0, j + 1).join('/');
    $('#siteBreadcrumb ol.breadcrumb').append('<li><a href="' + link + '">' + pageName.replace(/\.(htm[l]?|asp[x]?|php|jsp)$/, '') + '</a></li>');
    parts.push({
      "text": pageName,
      "link": link
    });
  }
}*/</script>
</div>
    <!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <ol style="float: left;">
            <li><a href="index.php">Home</a></li>
            <li><a href="symptomcheck.php">Symptom Check</a></li>
          </ol>
        </div>

      </div>
    </section><!-- Breadcrumbs Section -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
    <div class="row" >
        <div class="col-lg-3 col-md-4" >
            <div class="text-center card-box" style="height: 97%;">
                <div class="member-card">
                    <div class="thumb-xl member-thumb m-b-10 center-block">
                      <div class="frame">
                      
                  
            
<img src="profilepics/d41d8cd98f00b204e9800998ecf8427e1671617876.jpg" class="img-circle img-thumbnail" alt="profile-image">
                      </div>

                
                  <div style="margin-bottom: -10%;">
                   <center>
                    <p class="text-muted font-13" style="margin-top: 10px; color: white;"> <span class="m-l-15">Choose Dog</span></p>
                    
                      <a href="userprofile.php"><button type="button" class="btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light" style="background-color: #38b6ff; border-style: none;">Switch Dog</button></a>
                    
                  </center>
                    
                    </div>
                    <div class="text-left m-t-40" style="text-align: justify;">
                      
                       
                        <!-- <p class="text-muted font-13">Email : <span class="m-l-15"></span>
                        <?php  if (isset($_SESSION['user'])) : ?>
                          <strong><?php echo $_SESSION['user']['email']; ?></strong>
                          <small>
                            <br>
                          </small>
                        <?php endif ?> -->
                    </p>
                        
                    </div>
                </div>
              </div>
            </div> <!-- end card-box -->
        </div> <!-- end col -->


        <div class="col-md-8 col-lg-9">
            <div class="">
                <div class="">
                    <ul class="nav nav-tabs navtab-custom">
                        <li class="active">
                            
                        </li>
                    </ul>
                        <div class="tab-pane active" id="profile" >

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script>
        // Ignore this in your implementation
        window.isMbscDemo = true;
    </script>

    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>

    <!-- Mobiscroll JS and CSS Includes -->
    <link rel="stylesheet" href="css/mobiscroll.jquery.min.css">
    <script src="js/mobiscroll.jquery.min.js"></script>

    <style type="text/css">
            body {
        margin: 0;
        padding: 0;
    }

    button {
        display: inline-block;
        margin: 5px 5px 0 0;
        padding: 10px 30px;
        outline: 0;
        border: 0;
        cursor: pointer;
        background: #5185a8;
        color: #fff;
        text-decoration: none;
        font-family: arial, verdana, sans-serif;
        font-size: 14px;
        font-weight: 100;
    }

    input {
        width: 100%;
        margin: 0 0 5px 0;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 0;
        font-family: arial, verdana, sans-serif;
        font-size: 14px;
        box-sizing: border-box;
        -webkit-appearance: none;
    }

    .mbsc-page {
        padding: 1em;
    }

    </style>

</head>

<body>


    <div mbsc-page class="demo-multiple-select">
        <div>
         <h4>What are the Symptoms?</h4> 
         <form action="symptomcheck.php?symppetID=<?php echo htmlentities ($pid);?> " method="POST">
         
        <label>
        <input mbsc-input id="demo-multiple-select-input" placeholder="Select Symptoms'" data-dropdown="true" data-input-style="outline" data-label-style="stacked" data-tags="true" />
    </label>
     <select name="symptom[]" id="demo-multiple-select" multiple>
        <option value="1">Abdominal bloating</option>
        <option value="2">Abortion</option>
        <option value="3">Agression</option>
        <option value="4">Anemia</option>
        <option value="5">Bad smell</option>
        <option value="6">Biting their rear end</option>
        <option value="7">Bleeding</option>
        <option value="8">Blood in pee/poop</option>
        <option value="9">Blood in urine</option>
        <option value="10">Bloody diarrhea</option>
        <option value="11">Bloody stool</option>
        <option value="12">Itching</option>
        <option value="13">Licking</option>
        <option value="14">Losing Hair</option>
        <option value="15">Pain when pooping</option>
        <option value="16">Pale Gums</option>
        <option value="17">Avoiding Touch</option>
        <option value="18">Coughing</option>
        <option value="19">Difficulty breathing</option>
        <option value="20">Fever</option>
        <option value="21">Gagging</option>
        <option value="22">Loss of Appetite</option>
        <option value="23">Peeing more often</option>
        <option value="24">Runny Nose</option>
        <option value="25">Sneezing</option>
        <option value="26">Straining</option>
        <option value="27">Urine with blood or mocus</option>
        <option value="28">Vocalizing while trying to urinate</option>
        <option value="29">Vommiting/retching</option>
    </select>
    <button style="border-radius: 10px; float: right; margin-right: -105px; margin-top: -65px;" type="submit" name= "submit_symptom" href=
    "userprofile.php">Submit</button>
    </form>
    <center><div class="result" style="background-color: white; width: 100%; margin-top: 5%; height: 700px;"> 
    <h3 style="padding: 10px; font-weight: bold; color:#292f33; ">Conditions</h3><hr style="margin-top: -10px; width: 80%;">
   
    <?php

if(isset($_POST['submit_symptom']))
{
  
  $arraysymp=array();

  if(!empty($_POST['symptom'])){ 
    foreach($_POST['symptom'] as $selected_symp){
      switch ($selected_symp) {
        case "1":
          array_push( $arraysymp,"S001"); 
         
            
           break;
           case "2":
            array_push( $arraysymp,"S002"); 
           
              
             break;
             case "3":
              array_push( $arraysymp,"S003"); 
             
                
               break;
    
        case "4":
         array_push( $arraysymp,"S004"); 
        
           
          break;
        case "5":
          array_push($arraysymp, "S005");
          
          break;
        case "6":
          
          array_push( $arraysymp,"S006");
          break;
          case "7":
            array_push( $arraysymp,"S007");
            break;
          case "8":
            array_push( $arraysymp,"S008");
            break;
          case "9":
            array_push( $arraysymp,"S009");
            break;
            case "10":
          
              array_push( $arraysymp,"S010");
              break;
              case "11":
                array_push( $arraysymp,"S011");
                break;
              case "12":
                array_push( $arraysymp,"S012");
                break;
              case "13":
                array_push( $arraysymp,"S013");
                break;
                case "14":
                  array_push( $arraysymp,"S014"); 
                 
                    
                   break;
                   case "15":
                    array_push( $arraysymp,"S015"); 
                   
                      
                     break;
                     case "16":
                      array_push( $arraysymp,"S016"); 
                     
                        
                       break;
            
                case "17":
                 array_push( $arraysymp,"S017"); 
                
                   
                  break;
                case "18":
                  array_push($arraysymp, "S018");
                  
                  break;
                case "19":
                  
                  array_push( $arraysymp,"S019");
                  break;
                  case "20":
                    array_push( $arraysymp,"S020");
                    break;
                  case "21":
                    array_push( $arraysymp,"S021");
                    break;
                  case "22":
                    array_push( $arraysymp,"S022");
                    break;
                    case "23":
                  
                      array_push( $arraysymp,"S023");
                      break;
                      case "24":
                        array_push( $arraysymp,"S024");
                        break;
                      case "25":
                        array_push( $arraysymp,"S025");
                        break;
                      case "26":
                        array_push( $arraysymp,"S026");
                        break;
                        case "27":
                          array_push( $arraysymp,"S027");
                          break;
                        case "28":
                          array_push( $arraysymp,"S028");
                          break;
                        case "29":
                          array_push( $arraysymp,"S029");
                          break;
        
        
        default:
          echo "Symptom not Found in database";
        }

    }

  }
   
  
  
  include "config.php";

// Separate Array by " , "
$names_str = implode(" , ",$arraysymp);

// Insert record
$sql = "INSERT INTO history(userID, dogID, sympID) VALUES('".$_SESSION['user']['id']."','$pid','".$names_str."')";
mysqli_query($connection,$sql);

$sql = mysqli_query($connection,"SELECT * FROM history ORDER BY historyID DESC LIMIT 1");

$result['condition']= array();
$result['symptom']= array();
$results= array();
$sympres=array();
$sympD = array();

while($row = mysqli_fetch_assoc($sql)){
 
   $sympID = $row['sympID'];
  
   $name_explode = explode(" , ",$row['sympID']);  
    //
   foreach($name_explode as $condi){
   $sqlcond = mysqli_query($connection, "SELECT sympxcond.sympID, conditions.condition FROM sympxcond INNER JOIN conditions ON sympxcond.conID=conditions.conID where sympxcond.sympID = '".$condi."'");
   
  // echo "<table>";
   while($row = mysqli_fetch_array($sqlcond)){   //Creates a loop to loop through results
    
  // echo "<tr><td>" . htmlspecialchars($row['sympID']) . "</td><td>" . htmlspecialchars($row['condition']) . "</td></tr>";  //$row['index'] the index here is a field name
  // echo "</table>"; //Close the table in HTML
   array_push($sympres, $row['sympID']);
   array_push($results, $row['condition']);
   array_push($result['condition'], $row['condition']);
  

  }
  
   
  
   
}
foreach($sympres as $sympfor){
  $sqlsymp = mysqli_query($connection, "SELECT symptoms.sympID, symptoms.symptom FROM symptoms where sympID = '".$sympfor ."'");
  while($row = mysqli_fetch_array($sqlsymp)){
    array_push($sympD, $row['symptom']);
    array_push($result['symptom'], $row['symptom']);
    
  }
}
 // print_r($sympD);
  //print_r($results);
$percentage = array();

   function array_avg($results, $round=1){
    $num = count($results);
    return array_map(
        function($val) use ($num,$round){
         
            return array('count'=>$val,'avg'=>round($val/$num*100, $round));
        },
        array_count_values($results));
}

$avgs = array_avg($results);

/*
 * You can access any average/count like:
 * echo "The count of 3 is: {$avgs[3]['count']}";
 * 
 */

//echo '<pre>'.print_r($avgs,1).'</pre>';
//echo "The average of 3 is: {$avgs['Ehrlichiosis']['avg']} <br>";

// for ($x = 0; $x < count($results); $x++){
// 	echo "{$avgs[$results]['avg']}";
	
}
// foreach($results as $resperc){
//   echo "{$avgs[$resperc]['avg']} <br>";
// }
// foreach($sympD as $sympcol){
//   echo "$sympcol <br>";
// }
// foreach($avgs as $avgs1){
//   echo "$sympcol <br>";
// }
    ?>
<!DOCTYPE html>
<html>
  <head>
    <style>
      .tableFixHead {
        overflow-y: auto;
        height: 500px;
       
      }
      .tableFixHead thead th {
        position: sticky;
        top: 0;
    
      }
      table {
        border-collapse: collapse;
        width: 90%;
      }
      th,
      td {
        padding: 8px 16px;
        border: 1px solid #ccc;
        border-radius:5px; 
      }
      th {
        background: #38b6ff;
        color:white;
      }
    </style>
  </head>
  <body>
    <div class="tableFixHead">
      <table>
        <thead>
        

  

          <tr>
            <th>Symptoms</th>
            <th>Condtions</th>
            <th>Percentage</th>
          </tr>
        </thead>
        <tbody>
          
          
          <tr>
            <td> <?php foreach($sympD as $res){ echo " $res<br>";}?>   </td>
            <td> <?php foreach($results as $sympcol){ ?> <a  ><?php echo "$sympcol <br>";}?></a> </td>
            <td> <?php foreach($results as $resperc){ echo "{$avgs[$resperc]['avg']} %<br>";} ?></td>
          </tr>
          
          
        </tbody>
        
</table>
<section id="lifestage" style="margin-top: 50px; ">
<br>

<div style="padding: 20px; margin-left: 560px;"><label for="filter"style="font-size: 16px; ">Search:</label> <input type="text" name="Search" value="" id="filter" style="font-size: 16px;"/></div>

  <body id="index" TOPMARGIN=0 LEFTMARGIN=0>
      
        <table cellpadding="2px" cellspacing="0px" id="resultTable" width=100%>
          <thead>
            <tr>
              <th style="width:20%">Conditions<!-- <img src="data:image/gif;base64,R0lGODlhFQAJAIABAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4wLWMwNjAgNjEuMTM0Nzc3LCAyMDEwLzAyLzEyLTE3OjMyOjAwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjAxODAxMTc0MDcyMDY4MTE4OEM2REYyN0ExMDhBNDJFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdCNTAyODcwMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNTAyODZGMEY4NjExRTBBMzkyQzAyM0E1RDk3RDc3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDE4MDExNzQwNzIwNjgxMTg4QzZERjI3QTEwOEE0MkUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQBAAABACwAAAAAFQAJAAACF4yPgMsJ2mJ4VDKKrd4GVz5lYPeMiVUAADs"> --></th>
              <th style="width:50%">Tips</th>
              <th style="display:none;">Instruments</th>
            </tr>
          </thead>
          <tbody>
<tr class="ErrorRow">
<td>Allergies - Flea</td>
<td>1. Break the cycle. Wash your bedding, pet's bedding and rugs with detergent and warm water. Then, vacuum your carpets, rugs, chair and sofa cushions to remove fleas, flea eggs, and larvae.
<br><br> 2. Avoid irritants. Don’t use flea shampoo or other topical flea products without talking to your pet’s veterinarian first. <br><br>3. Stay in touch. Get regular checkups to your pet's veterinarian.</td>


<td style="display:none;">41845932,41845946,67333174,22516799,67128343,67130755,41845967,67133341
  ,67128008,67270409,41845956,67129452,67423456,41845922,68982124,22517166,41845936
  ,41845964,41845931,67332519,41845945,67671961,67681200,41845959,41845921,41845951,
  41845925,67655438,41845935,74769720,22517165,22516797,41845919,67128002,41845948,
  41845920,41845961,67128526,41845958,41845930,89325077,41845965,101658131,67822257,
  67109388,41845939,41845950,41845924,22517171,67123993,41845934,67332810,22516798,105080761,
  67895408,41845947,89647366,41845962,41845957,67529641,41845937,41845933</td>
</tr>

<tr class=ErrorRow>
<td>Allergies - Food</td>
<td>1. Avoid table food. The most common allergens are beef, dairy, wheat, egg, chicken, lamb, soy, pork, rabbit, and fish.</td>

<td style="display:none;">43430678,34945861,31550388,32851047,39335188,36044889,43929049,36523603,35887427,36247660,32738026</td>
</tr>

<tr class=ErrorRow>
<td>Anal Sac Disease</td>
<td>1. Prevention. Put your dog on a healthy diet and make sure they get plenty of exercise. <br><br> 2. Contact Vet. The treatment will be given based on the severity of the disease. </td>

<td style="display:none;">67157725,73299305</td>
</tr>

<tr class=ErrorRow>
<td> Bladder Stones</td>
<td>1. Check for genitals. Look for redness, swelling, signs of scratching or biting. <br><br>2. Water Intake. Keep your dog well-hydrated. If not readily drink water offer them a hig moisture diet (canned food). <br><br>3. Don't use DIY treatment. Do not give treatment to your dog unless a vet prescribe or consult it. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Bone Cancer</td>
<td>1. Give them regular exercise. It keeps bones strong, as well as the tissues that surround the bones.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Bronchitis</td>
<td>1. Give your dog a rest, warmth and proper hygiene.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Cushing's Disease</td>
<td>1. Stay in touch. Must have an adequate monitoring and follow-up checkups.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Cancer</td>
<td>1. Healthy weight. By getting a good-quality food and activity helps your dog to have a healthy body weight. <br><br> 2. Make an appointment. Make sure to schedule an appointment as soon as possible. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Canine Herpes - Adult Dogs</td>
<td>1. Preventing. Isolate pregnant dogs for three weeks before and after delivery. - Isolate puppies for three weeks after birth. - Keep the puppies warm (above 95 degrees) </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Canine Herpes - Puppies</td>
<td>1. Best protection. People in contact with dogs during this time should wash their hands thoroughly and often </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Deafness</td>
<td>1. Keep Track. Put a bell on the collar and a tag stating ''Deaf'' including your information.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Demodetic mange</td>
<td>1. Keep pets away from any other animals suspected to have mange, including avoiding public dog parks or similar areas that may foster contagious outbreaks.</td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Ear Infection</td>
<td>1. Keep the Ears Dry. Cleansing and drying the ears every five to ten days would be the best for prevention of this infection.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Ehrlichiosis</td>
<td>1. • Keep your grass nice and short and keep the dog away from bushy areas. <br><br> 2. Ensure your dog is not exposed to ticks. <br><br> 3. Check your dog regularly for ticks by running your fingers through their coat, on the skin. Pay attention to the head, neck, ears, chest, between their toes and around their mouths and gums. </td>

<td style="display:none;">5523749,34082255</td>
</tr>

<tr class=ErrorRow>
<td>Fecal impaction/constipation</td>
<td>1. Give the dog regular exercise. 2. Avoid sudden change of diet. <br><br> 3. Be aware of what the dog try to ingest: toys, gravel, plants, dirt and bones. <br><br> 4. Consult to the vet what diet have a high in fiber. </td>

<td style="display:none;">5523749,34082255</td>
</tr>
<tr class=ErrorRow>
<td>Folliculitis</td>
<td>1. Stay up to date on flea and tick prevention. Treat your dog for fleas, ticks, mites, and anything that could potentially infect their skin. <br><br> 2. When you notice any skin irritation it is the best to get them to the vet.  </td>

<td style="display:none;">5523749,34082255</td>
</tr>
    </div>
  </body>
  </html>

</section>
      </div>
      
        </tbody>
        
      </table>
      
    </div>
  </body>
</html>
  <?php
}



 


?>








    </div>
  </center>
    </div>
    </div>

    <script>
        
            mobiscroll.setOptions({
        locale: mobiscroll.localeEn,                                             // Specify language like: locale: mobiscroll.localePl or omit setting to use default
        theme: 'ios',                                                            // Specify theme like: theme: 'ios' or omit setting to use default
            themeVariant: 'light'                                                // More info about themeVariant: https://docs.mobiscroll.com/5-20-0/select#opt-themeVariant
    });
    
    $(function () {
        // Mobiscroll Select initialization
        $('#demo-multiple-select').mobiscroll().select({
            inputElement: document.getElementById('demo-multiple-select-input')  // More info about inputElement: https://docs.mobiscroll.com/5-20-0/select#opt-inputElement
        });
    });
    </script>
  </div>
</div> <!-- end col -->
</div>
    <!-- end row -->
</div>
</div>
</div>
</section><!-- End Portfolio Details Section -->
</main><!-- End #main -->

  

    
  <!-- Vendor JS Files -->
  <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>