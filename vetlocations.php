<?php 
	include('functions.php');
	if (!isLoggedIn()) {
	$_SESSION['msg'] = "You must log in first";
	header('location: login.php');
}
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/vendor/leaflet.css">
    <link rel="stylesheet" href="assets/css/main.css">
	<title>Symptom Check</title>
</head>

<style>

body{
	width: 97%;
    margin: 2%;
    background-image: url(images/userP_BG_left.jpg);
    background-size: cover;
}
</style>
<body>
<main>
        <div id="myMap"></div>
             
    </main>
    
    
    <script src="assets/vendor/leaflet.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>