<?php include('functions.php');
error_reporting (E_ALL ^ E_NOTICE); 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
   	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>User Registration</title>
	<link rel="stylesheet" href="style.css">
</head>
<body style="background-image: url(images/bg.png); background-size: cover; font-family: sans-serif;">

<form method="post" action="index.php" style="margin-top: 100px; background-color: #FFFFF0; ">
	<?php echo display_error(); ?>
	<div class="input-group">
		<label>Username</label>
		<input type="text" name="username" value="<?php echo $username; ?>">
	</div>
	<div class="input-group">
		<label>Fullname</label>
		<input type="text" name="fullname" value="<?php echo $fullname; ?>">
	</div>
	<div class="input-group">
		<label>Mobile #</label>
		<input type="tel" name="mobile" value="<?php echo $mobile; ?>">
	</div>
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email" value="<?php echo $email; ?>">
	</div>
	<div class="input-group">
		<label>Address</label>
		<input type="text" name="address" value="<?php echo $address; ?>">
	</div>
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password_1">
	</div>
	<div class="input-group">
		<label>Confirm password</label>
		<input type="password" name="password_2">
	</div>
	<div class="input-group">
		<center><a href="index.php">
			<button type="submit" class="btn" name="register_btn">Register</button></a></center>
	</div>
	<center>
	<p>
		Already a member? <a href="login.php">Sign in</a>
	</p>
</center>
</form>
</body>
</html>